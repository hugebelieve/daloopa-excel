Problem Statement

Design a datagrid component that renders a CSV dataset.

Component should take columns, data, and pagination info.
You should be able to render custom cell (i.e. changing the color, manipulating the cell value etc)
Handle click on a cell.
Pagination in the table should be implemented.


Download the data

Grab the data from here: https://pkgstore.datahub.io/core/nyse-other-listings/other-listed_csv/data/9f38660d84fe6ba786a4444b815b3b80/other-listed_csv.csv
This is a list of companies with their public information such as ticker and company name.



Notes:

Please feel free to google framework / language documentations if you need to.
During your Zoom interview, please share your whole screen while working on this project so that we can see how you work with code. 
Do not use ChatGPT or any LLM-based chatbot / coding assistant.
 


