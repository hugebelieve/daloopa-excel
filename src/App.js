import React from "react";
import { NativeBaseProvider, Box, Heading } from "native-base";
import Excel from './components/Excel/Excel';

const sampleCSV = 'https://pkgstore.datahub.io/core/nyse-other-listings/other-listed_csv/data/9f38660d84fe6ba786a4444b815b3b80/other-listed_csv.csv'

export default function App() {
  return (
    <NativeBaseProvider>
      <Heading>Data Grid</Heading>
      <Box>Hello world</Box>
      <Excel url={sampleCSV} />
    </NativeBaseProvider>
  );
}
