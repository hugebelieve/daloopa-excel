export const fetchIO = async (url) => {
    const response = await fetch(url)
    return response.text()
}