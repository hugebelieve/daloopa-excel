import { useEffect, useState } from "react";
import { csvParser } from "../../io/csv-parser";
import { fetchIO } from "../../io/fetch";
import DataGrid from "../DataGrid/DataGrid";

const onExcelMount = async function(url) {
    const fetchText = await fetchIO(url);
    const {columns, data} = csvParser(fetchText);
    return {columns, data}
}

export default function Excel({url}) {
    const [excelData, setColumnsData] = useState({columns: [], data: []});
    useEffect(() => {
       onExcelMount(url).then(({columns, data}) => {
        setColumnsData({columns, data})
       });
    }, [url])
    return (
        <DataGrid columns={excelData.columns} data={excelData.data} pagination={10} />
    )
}
