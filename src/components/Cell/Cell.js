import { Box } from "native-base";
import { useCallback, useState } from "react";

export default function Cell ({value, bgColor, __identity}) {
    const [cellValue, setCallValue] = useState(value);
    return (
        <Box
            width={'10vw'}
            height={'5vh'} borderColor={'black'} backgroundColor={bgColor} borderWidth={1} overflow={'hidden'}
            padding="4px">

                <label __identity={__identity}>{value}</label>
            </Box>
    )
}