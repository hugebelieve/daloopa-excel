import {Box, Flex} from 'native-base';
import { useCallback, useState } from 'react';
import Cell from '../Cell/Cell';

export default function DataGrid({
    columns, data, pagination
}) {
    // temp state data
    const [selectedCell, setSelectedCell] = useState('');
    const onGridItemClick = useCallback((event) => {
        let identityKey = event.target?.getAttribute('__identity')
        if (!identityKey) {
            identityKey = event.target?.parent?.getAttribute('__identity')
        }
        console.log(event.target, event.target?.parent, event)
        if (identityKey) {
            setSelectedCell(identityKey)
        }
    }, [])

    const onInputSubmit = useCallback((event) => {
        console.log(event)

    }, [])
    return (
        <div onClick={onGridItemClick}>
            {
                selectedCell ? <input onSubmit={onInputSubmit} /> : null
            }
            <Flex direction={'row'}>
                {
                    columns.map((column) => {
                        return <Cell key={column} value={column} />
                    })
                }
            </Flex>
            {
                data.slice(0, pagination).map((row, rowIndex) => {
                    return (
                        <Flex key={rowIndex} direction={'row'}>
                            {
                                row.map((column, index) => {
                                    const identity = `${rowIndex}-${index}`
                                    return <Cell __identity={identity}
                                            bgColor={identity === selectedCell ? 'blue.300' : undefined}
                                            key={column + index}
                                            value={column} />
                                })
                            }
                        </Flex>
                    )
                })
            }

        </div>
    )
}